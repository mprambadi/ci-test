const sum = require('../app/sum')

test("add one and two will output three", () => {
    expect(sum(1,2)).toBe(3);
})

test('add three and five should return eight', () => {
    expect(sum(3,5)).toBe(8);
})